Source: nxt-firmware
Section: electronics
Priority: optional
Maintainer: Debian LEGO Team <debian-lego-team@lists.alioth.debian.org>
Uploaders:
 Dominik George <nik@naturalnet.de>,
 Petter Reinholdtsen <pere@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 gcc-arm-none-eabi,
 libnewlib-arm-none-eabi,
Standards-Version: 4.6.0
Homepage: http://nxt-firmware.ni.fr.eu.org/
Vcs-Git: https://salsa.debian.org/debian-lego-team/nxt-firmware.git
Vcs-Browser: https://salsa.debian.org/debian-lego-team/nxt-firmware
Rules-Requires-Root: no

Package: nxt-firmware
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Recommends:
 libnxt,
Suggests:
 nbc,
Description: Improved firmware for LEGO Mindstorms NXT bricks
 The NXT Improved Firmware is a community-based open source project
 around the original LEGO Mindstorms firmware for the NXT
 bricks.
 .
 The firmware can be installed on the robot control bricks of type NXT.
 It is almost identical to the original firmware, meaning that
 all existing software working with the original firmware can be expected
 to work with the improved firmware as well.
 .
 The main differences between the original firmware and the improved
 firmware are the addition of absolute position regulation for motors and
 that it can be built with GCC rather than the non-free toolchain used by
 the LEGO Group.
 .
 This package contains the firmware image file, which can be flashed onto
 the brick with various tools, e.g. fwflash from the libnxt package.
